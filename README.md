# Full Stack FastAPI and PostgreSQL
### run docker
```
docker-compose up -d
```

### `exec` inside the running container
```
docker-compose exec backend bash
```

[![API docs](img/login.png)]