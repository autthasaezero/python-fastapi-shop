from typing import List

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.shop import Shop
from app.schemas.shop import ShopCreate, ShopUpdate


class CRUDShop(CRUDBase[Shop, ShopCreate, ShopUpdate]):
    def get_shop(
        self, db: Session, *, skip: int = 0, limit: int = 100
    ) -> List[Shop]:
        return (
            db.query(self.model)
            .offset(skip)
            .limit(limit)
            .all()
        )


shop = CRUDShop(Shop)
