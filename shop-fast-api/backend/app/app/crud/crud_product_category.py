from typing import List
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase
from app.models.product_category import ProductCategory
from app.schemas.product_category import ProductCategoryCreate, ProductCategoryUpdate


class CRUDProductCategory(CRUDBase[ProductCategory, ProductCategoryCreate, ProductCategoryUpdate]):
    pass


product_category = CRUDProductCategory(ProductCategory)
