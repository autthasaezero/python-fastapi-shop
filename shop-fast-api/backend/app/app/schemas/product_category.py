
from pydantic import BaseModel


# Shared properties
class ProductCategoryBase(BaseModel):
    category_name: str
    description: str


# Properties to receive on item creation
class ProductCategoryCreate(ProductCategoryBase):
    category_name: str
    description: str


# Properties to receive on item update
class ProductCategoryUpdate(ProductCategoryBase):
    pass


# Properties shared by models stored in DB
class ProductCategoryInDBBase(ProductCategoryBase):
    id: int
    category_name: str
    description: str
    
    class Config:
        orm_mode = True

# Properties to return to client
class ProductCategory(ProductCategoryInDBBase):
    pass



# Properties properties stored in DB
class ProductCategoryInDB(ProductCategoryInDBBase):
    pass

