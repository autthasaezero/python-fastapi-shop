from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel
from ..models import Product


# Shared properties
class ShopBase(BaseModel):
    shop_name : str
    phone_number : str
    address : str
    description : str


# Properties to receive on item creation
class ShopCreate(ShopBase):
    shop_name: str
    phone_number: str
    address: str
    description: str


# Properties to receive on item update
class ShopUpdate(ShopBase):
    pass


# Properties shared by models stored in DB
class ShopInDBBase(ShopBase):
    id: int
    shop_name: str
    phone_number: str
    address: str
    description: str
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True

# Properties to return to client
class Shop(ShopInDBBase):
    pass



# Properties properties stored in DB
class ShopInDB(ShopInDBBase):
    pass

