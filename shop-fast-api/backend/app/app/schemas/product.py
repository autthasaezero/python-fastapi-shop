from datetime import datetime
from typing import Any, Optional
from pydantic import BaseModel
from ..models.product_category import ProductCategory
from ..models.product import Product


# Shared properties
class ProductBase(BaseModel):
    shop_id: int
    product_category_id : int
    name: str
    price: float
    quantity: int
    description: str


# Properties to receive on item creation
class ProductCreate(ProductBase):
    shop_id: int
    product_category_id: int
    name: str
    price: float
    quantity: int
    description: str


# Properties to receive on item update
class ProductUpdate(ProductBase):
    pass


# Properties shared by models stored in DB
class ProductInDBBase(ProductBase):
    id: int
    shop_id: int
    product_category_id : int
    name: str
    price: float
    quantity: int
    description: str
    created_at: datetime
    updated_at: datetime
    
    class Config:
        orm_mode = True

# Properties to return to client
class Product(ProductInDBBase):
    product_category :  Optional[Any] = None
    shop: Optional[Any] = None


# Properties properties stored in DB
class ProductInDB(ProductInDBBase):
    pass

