from fastapi import APIRouter

from app.api.api_v1.endpoints import items, login, users, utils, shops, product, product_category

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(items.router, prefix="/items", tags=["items"])
api_router.include_router(shops.router, prefix="/shops", tags=["shops"])
api_router.include_router(product.router, prefix="/products", tags=["products"])
api_router.include_router(product_category.router, prefix="/product_categories", tags=["product_categories"])