from typing import Any, List

from sqlalchemy.sql.operators import desc_op
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from fastapi_pagination import Page, PaginationParams
from fastapi_pagination.ext.sqlalchemy import paginate

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=Page[schemas.Product])
def read_products(
    db: Session = Depends(deps.get_db),
    params: PaginationParams = Depends()
) -> Any:
    """
    List products.
    """
    return paginate(db.query(models.Product).order_by(desc_op(models.Product.created_at)), params)


@router.get("/{id}", response_model=schemas.Product)
def read_product(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
) -> Any:
    """
    Read product.
    """
    product = crud.product.get(db=db, id=id)
    if not product:
        raise HTTPException(status_code=404, detail="Product not found")
    return product


@router.put("/{id}", response_model=schemas.Product)
def update_product(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    item_in: schemas.ProductUpdate,
) -> Any:
    """
    Update an product.
    """
    product = crud.product.get(db=db, id=id)
    if not product:
        raise HTTPException(status_code=404, detail="product not found")
    product = crud.product.update(db=db, db_obj=product, obj_in=item_in)
    return product


@router.delete("/{id}", response_model=schemas.Product)
def delete_product(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
) -> Any:
    """
    Update an product.
    """
    product = crud.product.get(db=db, id=id)
    if not product:
        raise HTTPException(status_code=404, detail="Product not found")
    product = crud.product.remove(db=db, id=id)
    raise HTTPException(status_code=201, detail="Product delete")


@router.post("/", response_model=schemas.Product)
def create_product(
    *,
    db: Session = Depends(deps.get_db),
    item_in: schemas.ProductCreate,
) -> Any:
    """
    Create new shop.
    """
    shop = crud.product.create(db=db, obj_in=item_in)
    return shop