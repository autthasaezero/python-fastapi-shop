from typing import Any, List

from sqlalchemy.sql.operators import desc_op

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from fastapi_pagination import Page, PaginationParams
from fastapi_pagination.ext.sqlalchemy import paginate

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=Page[schemas.Shop])
def read_shops(
    db: Session = Depends(deps.get_db),
    params: PaginationParams = Depends()
) -> Any:
    """
    List shops.
    """
    return paginate(db.query(models.Shop).order_by(desc_op(models.Shop.created_at)), params)


@router.get("/{id}", response_model=schemas.Shop)
def read_shop(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
) -> Any:
    """
    Read shops.
    """
    shop = crud.shop.get(db=db, id=id)
    if not shop:
        raise HTTPException(status_code=404, detail="Shop not found")
    return shop

@router.get("/{id}/product", response_model=Page[schemas.Product])
def read_shop_product(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    params: PaginationParams = Depends()
) -> Any:
    """
    Read product in shops.
    """
    return paginate(db.query(models.Product).filter_by(shop_id=id).order_by(desc_op(models.Product.created_at)), params)


@router.put("/{id}", response_model=schemas.Shop)
def update_shop(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    item_in: schemas.ShopUpdate,
) -> Any:
    """
    Update an shop.
    """
    shop = crud.shop.get(db=db, id=id)
    if not shop:
        raise HTTPException(status_code=404, detail="Shop not found")
    shop = crud.shop.update(db=db, db_obj=shop, obj_in=item_in)
    return shop


@router.post("/", response_model=schemas.Shop)
def create_shop(
    *,
    db: Session = Depends(deps.get_db),
    item_in: schemas.ShopCreate,
) -> Any:
    """
    Create new shop.
    """
    shop = crud.shop.create(db=db, obj_in=item_in)
    return shop