from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from fastapi_pagination import Page, PaginationParams
from fastapi_pagination.ext.sqlalchemy import paginate

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=Page[schemas.ProductCategory])
def read_product_categories(
    db: Session = Depends(deps.get_db),
    params: PaginationParams = Depends()
) -> Any:
    """
    List product categories.
    """
    return paginate(db.query(models.ProductCategory), params)

