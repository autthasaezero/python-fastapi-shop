from typing import TYPE_CHECKING

from sqlalchemy import Boolean, ForeignKey, Column, Integer, String, DECIMAL, Text, DateTime, text, func
from sqlalchemy.orm import relationship
from app.db.base_class import Base



class Product(Base):
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    shop_id = Column(Integer, ForeignKey("shop.id"))
    product_category_id = Column(Integer, ForeignKey("productcategory.id"))
    name = Column(String, index=False)
    price = Column(DECIMAL, nullable=False)
    quantity = Column(Integer, nullable=False)
    description = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())
    shop = relationship("Shop")
    product_category = relationship("ProductCategory")