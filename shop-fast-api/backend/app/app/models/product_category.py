from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class ProductCategory(Base):
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    category_name = Column(String, index=True, nullable=False)
    description = Column(Text, nullable=True)
    product_category = relationship("Product")

