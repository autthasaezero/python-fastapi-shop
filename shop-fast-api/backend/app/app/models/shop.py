from typing import TYPE_CHECKING

from sqlalchemy import Boolean, func, text, Column, Integer, String, Text, DateTime
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Shop(Base):
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    shop_name = Column(String, index=True, nullable=False)
    phone_number = Column(String, nullable=False)
    address = Column(Text, nullable=True)
    description = Column(Text, nullable=True)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())

